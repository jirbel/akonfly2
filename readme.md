# QAKON

je treba mit nainstalovane php-cgi:
	- https://github.com/bezoerb/php2html#installing-php-cgi

# Instalace #

Install npm and bower dependencies
```
#!bash

npm install
```

```
#!bash

bower install
```
```
#!bash

gulp setup
```

## Build 

Jako posledni task pusti i watch
```
#!bash

gulp
```

## Stage 

zminifikuje .css, .js, spusti autoprefixer..

```
#!bash

gulp stage
```

# Info #
(prevazne pro jirku)

## Rozdily o proti predchozich Akon sablonach 

- jinaci struktura 
- vse se kompiluje ze src nebo bower_components, vcetne obrazku, js, less, html, fontu, favicon... do dist
- styly se uz nepisi do jednoho **.less** souboru
- minifikace obrazku
- kompilace php do html
- many more...

## Doporuceni 
- externi package/pluginy co pujdou, instalovat pres bower:
  napr.:

```
#!bash

bower install slick-carousel --save
```
  - nainstaluje balik do **bower_components**
  - "--save" ulozi do **bower.json** balik jako zavislost projektu
  - pokud nevis presne nazev repozitare, pouzij 
```
#!bash

bower search <name>
```

- pak je treba nalinkovat styly do **styles.less** a js do **gulpfile.js**, viz defaultni package

- ve **styles.less** zakomentovat toho co nejvic, vysledne css bude mit mensi velikost

- predevsim komentuj zbytecny bootstrap less, a nebudes zazivat takovej pain pri resetovani, prvne se ale porad s lukem, co vsechno bude z bootstrapu potrebovat

- muzes take ty bootstrap less upravit primo a opet se vyhnes resetovani

- to komentovani plati taky pro js, v **gulpfile.js** v paths.scripts.src mas cesty k js, opet zakomentuj to co nevyuzijes

- do slozky **dist** neni treba vubec zasahovat, vse se kompiluje z **src**

- neco me casem urcite jeste napadne

## Co spousti watch 

jakakoliv zmena/novy soubor - .less, .php, obrazky, fonty, .js, favicony

# Jak postupovat pri novem projektu #

vesmes podobny postup jak minule


stazeni repozitare


```
#!bash

npm install
```

```
#!bash

gulp setup
```

```
#!bash

gulp
```
 
webikony - [realfavicongenerator.net](http://realfavicongenerator.net/) - nahrat do slozky **src/favicons** a upravit cesty/barvy k faviconam v  **parts/head.php**

fonty (google web fonts, typekit - zeptej se zbynka, popripade konverze fontsquirell nebo www.web-font-generator.com)
 
**variables.less** - definovani promenych, prepsani bootstrap promennych, upravuj pouze **src/less/variables.less**, lepsi prehled o zmenach

typografie, doporucuju prvne zaklad napsat v HTML (p, h1, h2, h3, a, ul...), pak stylovat (**styles.less**, **basics.less**, trosku promazat bootstrap :D) 
 
**styles.less** - zbytecnosti dat dopici, **basics.less** - opakujici se elementy, drobne upravy, pomocne classy

BEMs - **src/less/bems/** - stylovani jednotlivych bloku
 
zakomentovani js v **gulpfile.js**
 
az to budes mit,
```
#!bash

gulp stage
```
v **head.php** a **bottom.php** upravit cesty k minifikovanym css/js

otestovat, saucelabs, atd.